package main

import (
	"fmt"
	"os"
	"os/exec"
)

func main(){
	file, err := os.Create("script.sh")

    if err != nil {
		fmt.Println(err)
		return
	}
	
	err = file.Chmod(0755)
	if err != nil {
		fmt.Println(err)
		return
	}
	
	text := "#!/bin/bash\necho Hello Worlt!"
	_, err = file.WriteString(text)
	if err != nil {
		fmt.Println(err)
		return
	}
	file.Close()

	cmd := exec.Command("./script.sh")
	output, err := cmd.Output()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(output))
	
}